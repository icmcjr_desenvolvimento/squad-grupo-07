const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

// inicializando o servidor
const server = express();

// Require pras rotas criadas anteriormente
const userRoutes = require('./api/userRoutes');
const listRoutes = require('./api/listRoutes');
const taskRoutes = require('./api/taskRoutes');
const testRoutes = require('./api/testRoutes');

// Usando CORS, Bodyparser e urlencoded
server.use(cors());
server.use(express.urlencoded({ extended: true }));
server.use(express.json());

// Acessando a base de dados no mongoose

mongoose.connect('mongodb://127.0.0.1:27017/TodoApp', // Porta padrão, checar se no seu computador é a mesma
  { // Ele reclama se não usar essas opções
    // Não vou questiona-lo
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

// Pedindo pro servidor usar as rotas da variável "routes", caso a requisição comece com /api/
// Ex: link.com/api/ -> Realiza um GET em '/' dentro das rotas definidas em ./api/routes.js
server.use('/api/users', userRoutes);
server.use('/api/lists', listRoutes);
server.use('/api/tasks', taskRoutes);
server.use('/api/test', testRoutes);

// Definindo a porta do nosso servidor e iniciando-o
const port = 5000;
server.listen(port, () => console.log('Iniciando na porta ' + port));
