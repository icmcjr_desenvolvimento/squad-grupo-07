const router = require('express').Router();
const Todo = require('../database/TodoSchema');

// ROTAS RELACIONADAS A TAREFAS

// POST
// PARAMS: [userID, listID], BODY: [taskTitle, taskDescription, taskPriority]
// Adiciona uma nova tarefa numa determinada lista e retorna a nova lista
router.post('/:userID/:listID', (req, res) => {
  // Criando a nova tarefa
  const newTask = {
    taskTitle: req.body.taskTitle,
    taskDescription: req.body.taskDescription,
    taskPriority: req.body.taskPriority
  };

  // Definindo os IDs, apenas para diminuir tamanho da query a seguir
  const [userID, listID] = [req.params.userID, req.params.listID];

  // Updata usando subquerys e o $push
  Todo.findOneAndUpdate(
    { _id: userID, 'lists._id': listID },
    { $push: { 'lists.$.tasks': newTask } },
    { new: true }
  )
    .then(data => res.send(data.lists)) // Retorna os novos dados
    .catch(error => res.send(error));
});

// PUT
// PARAMS: [userID, listID, taskID] BODY: [taskTitle, taskDescription, taskPriority, taskDate, taskConcluded]
// Altera os dados de uma lista e retorna a lista alterada
router.put('/:userID/:listID/:taskID', (req, res) => {
  // Desestruturação de variáveis que vão ser usadas
  const { userID, listID, taskID } = req.params;
  const { taskTitle, taskDescription, taskPriority, taskDate, taskConcluded } = req.body;

  // Query pro update
  Todo.findOneAndUpdate(
    { _id: userID }, // Filtro Inicial de usuário
    {
      $set: { // $set pra alterar
        'lists.$[i].tasks.$[j].taskTitle': taskTitle,
        'lists.$[i].tasks.$[j].taskPriority': taskPriority,
        'lists.$[i].tasks.$[j].taskDescription': taskDescription,
        'lists.$[i].tasks.$[j].taskDate': taskDate,
        'lists.$[i].tasks.$[j].taskConcluded': taskConcluded
      }
    },
    {
      arrayFilters: [ // Filtros adicionais usados acima
        { 'i._id': listID },
        { 'j._id': taskID }
      ],
      new: true
    })
    .then(data => res.send(data.lists))
    .catch(error => res.send(error));
});

// DELETE
// PARAMS: [userID, listID, taskID]
// Descrição: Deleta uma tarefa específica na base de dados
router.delete('/:userID/:listID/:taskID', (req, res) => {
  const { userID, listID, taskID } = req.params;

  Todo.findOneAndUpdate(
    { _id: userID },
    { $pull: { 'lists.$[i].tasks': { _id: taskID } } },
    {
      arrayFilters: [
        { 'i._id': listID }
      ],
      new: true
    }
  )
    .then(data => res.send(data.lists))
    .catch(error => res.send(error));
});

module.exports = router;
