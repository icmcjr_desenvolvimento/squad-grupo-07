const router = require('express').Router();
const Todo = require('../database/TodoSchema');

// ROTAS RELACIONADAS AS LISTAS

// POST
// PARAMS: [userID], BODY: [listTitle, listColor]
// Cria uma nova lista e retorna o novo conjunto de listas
router.post('/:userID', (req, res) => {
  const userID = req.params.userID;
  const newList = {
    listTitle: req.body.listTitle,
    listColor: req.body.listColor,
    tasks: []
  };

  Todo.findOneAndUpdate(
    { _id: userID },
    { $push: { lists: newList } },
    { new: true }
  )
    .then(data => res.send(data.lists))
    .catch(error => res.send(error));
});

// PUT
// PARAMS: [userID, listID] BODY: [listTitle, listColor]
// Altera os dados de uma lista e retorna a lista alterada
router.put('/:userID/:listID', (req, res) => {
  const [userID, listID] = [req.params.userID, req.params.listID];

  const listTitle = req.body.listTitle;
  const listColor = req.body.listColor;

  Todo.findOneAndUpdate(
    { _id: userID, 'lists._id': listID },
    { $set: { 'lists.$.listTitle': listTitle, 'lists.$.listColor': listColor } },
    { new: true }
  )
    .then(data => res.send(data.lists))
    .catch(error => res.send(error));
});

// DELETE
// PARAMS: [userID, listID]
// Deleta a lista em questão, retorna o conjunto de listas não deletados
router.delete('/:userID/:listID', (req, res) => {
  const [userID, listID] = [req.params.userID, req.params.listID];

  Todo.findOneAndUpdate(
    { _id: userID },
    { $pull: { lists: { _id: listID } } },
    { new: true }
  )
    .then(data => res.send(data.lists))
    .catch(error => res.send(error));
});

module.exports = router;
