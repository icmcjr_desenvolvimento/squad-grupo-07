const router = require('express').Router();
const Todo = require('../database/TodoSchema');

// Todo -> Manipular a base de dados
// Ex: Todo.findOne -> acha algo baseado em algum critério na base

// Endpoints / Rotas abaixo
// Lembrando que todas as rotas começam com /api
// Isso foi definido em server.js (linha 27)

/* _____   __ ________  _________ _     _____
|  ___\ \ / /|  ___|  \/  || ___ \ |   |  _  |
| |__  \ V / | |__ | .  . || |_/ / |   | | | |
|  __| /   \ |  __|| |\/| ||  __/| |   | | | |
| |___/ /^\ \| |___| |  | || |   | |___\ \_/ /
\____/\/   \/\____/\_|  |_/\_|   \_____/\___/ */

// metodo: Qual método foi usado na rota?
// parâmetros: Quais parametros devem ser usados na rota?
// descrição: O que esta rota faz?

/// ###################### USUARIO

// GET
// PARAMS: [username]
// Retorna as informações específicas de um usuário
router.get('/:username', (req, res) => {
  Todo.findOne({ username: req.params.username })
    .then((data) => res.send(data))
    .catch(error => res.send(error));
});

// POST
// BODY: [username]
// Cria um novo usuário no sistema, retorna o documento do novo usuário
router.post('/', async (req, res) => {
  const newUser = {
    username: req.body.username,
    lists: []
  };

  await Todo.create(newUser);

  Todo.findOne({ username: req.body.username })
    .then(data => res.send(data))
    .catch(error => res.send(error));
});

// DELETE
// PARAMS: [userID]
// Deleta um usuário, usando seu ID
router.delete('/:userID', (req, res) => {
  Todo.findOneAndDelete({ _id: req.params.userID })
    .then((data) => res.send(data))
    .catch(error => res.send(error));
});

// ################################## LISTA

module.exports = router;

// ? definir os protocolos de acesso aos dados sobre as listas
// get: consultar todas as informações
// post : inserir todas as informações
// put : editar -->cor, titulo, descrição, status, prioridade e data
// delete : remover o elemento
