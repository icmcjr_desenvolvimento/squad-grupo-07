const router = require('express').Router();
const Todo = require('../database/TodoSchema');

// ROTAS DE TESTE

// GET
// []
// Retorna a base completa, com todos os usuários
router.get('/', (req, res) => {
  Todo.find({}).then((data) => res.send(data));
});

// POST
// []
// Rota para adicionar dados básicos
router.post('/', (req, res) => {
  Todo.insertMany([
    {
      username: 'Luis-Jorge',
      lists: [
        {
          listTitle: 'Projeto todo daaaaaaaaaa Jr',
          listColor: 'yellow',
          taskQuantity: 1,
          tasks: []
        },
        {
          listTitle: 'Projeto aaaaaaa da Jr',
          listColor: 'yellow',
          taskQuantity: 1,
          tasks: []
        },
        {
          listTitle: 'aaaa todo da Jr',
          listColor: 'yellow',
          taskQuantity: 1,
          tasks: []
        },
        {
          listTitle: 'Projeto da aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
          listColor: 'blue',
          taskQuantity: 1,
          tasks: [
            {
              taskTitle: 'aprender express',
              taskPriority: 2, // 1 = Baixo, 2 = Medio, 3 = Alta
              taskDescription: 'assistir aula recomendada pelo pato'
            }
          ]
        }
      ]
    },
    {
      username: 'João-Guilherme',
      lists: [
        {
          listTitle: 'Projeto todo da Jr',
          listColor: 'yellow',
          taskQuantity: 1,
          tasks: [
            {
              taskTitle: 'fazer rotas express',
              taskPriority: 3, // 1 = Baixo, 2 = Medio, 3 = Alta
              taskDescription: 'codar o backend'
              // taskData:
              // taskConcluded: false
            }
          ]
        }
      ]
    }
  ]);
  res.send({ status: 200, result: 'funcionou' });
});

module.exports = router;
