const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// Definição do Schema da base de dados
// Não esta finalizado, é só um exemplo por enquanto.

// OBS: IDs Definidos pelo próprio mongoose
// Sempre que houver uma lista, o objeto dentro dela vai ter um ID a ser acessado

const todoSchema = new Schema([
  {
    username: { type: String, required: true },
    lists: [
      {
        listTitle: { type: String, required: true },
        listColor: { type: String, default: 'aliceblue' },
        tasks: [
          {
            taskTitle: { type: String, required: true },
            taskDescription: { type: String, default: '' },
            taskPriority: { type: Number, default: 1 }, // 1 = Baixo, 2 = Medio, 3 = Alta
            taskDate: { type: Date, default: Date.now() },
            taskConcluded: { type: Boolean, default: false }
          }
        ]
      }
    ]
  }
]);

module.exports = mongoose.model('Todo', todoSchema);
