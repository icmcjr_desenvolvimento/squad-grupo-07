Página Inicial

O sistema deve possuir uma página inicial estática, contendo as seguintes seções:
Imagem de fundo preenchendo a tela inteira com um botão Call to Action, para começar a utilizar o sistema.
Principais vantagens de se utilizar o sistema.
Feedback de algumas pessoas a respeito da utilização do sistema.
 A página inicial também deve conter um menu, sempre visível para navegar pelas seções.
 O menu também deve conter um botão Call to Action para começar a utilizar o sistema.
 A página inicial também deve conter um rodapé com links para as redes sociais da ICMC Jr.

Listas

 O sistema deve permitir a adição, edição e remoção de listas de tarefas, adicionando e alterando os seguintes dados: nome da lista e cor da lista.
 O sistema deve permitir a pesquisa entre as listas de tarefas, pesquisando entre os nomes das listas e as descrições das tarefas.
 O sistema deve exibir as listas de tarefas indicadas por suas cores.
 O sistema deve exibir um indicador de quantas tarefas foram realizadas daquela lista (Quantidade de tarefas realizadas / Quantidade total de tarefas da lista)

Tarefas

 O sistema deve permitir a adição, edição e remoção de tarefas para as listas, alterando os seguintes dados: descrição da tarefa e prioridade:
Vermelho (alta prioridade)
Amarelo (média prioridade) Azul (baixa prioridade)
 O sistema deve permitir a marcação de tarefas como realizadas.
