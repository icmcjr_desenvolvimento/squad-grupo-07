import React from 'react';

// import LinkBtn from '../linkbutton/index'
import './styles.css';

const ListH = (props) => {
  return (
    <div className='list-header' style={{ backgroundColor: props.color ? props.color : 'var(--secondary)' }}>
      <div className='title'>
        <h3 className='title-content'>{props.content}</h3>
        <div className='title-buttons'>
          <button onClick={props.onAdd} className='header-button plus'>
            <i className='fas fa-plus' />
          </button>
          <button onClick={props.onEdit} className='header-button undo'>
            <i className='fas fa-undo' />
          </button>
          <button onClick={props.onRemove} className='header-button minus'>
            <i className='fas fa-minus' />
          </button>
        </div>

      </div>
      <div className='list-header-body'>
        <input
          type='text'
          id='task-input-header'
          placeholder='Pesquise aqui...'
          value={props.inputValue}
          onChange={(event) => props.onInputChange(event)}
        />
      </div>
    </div>

  );
};

export default ListH;
