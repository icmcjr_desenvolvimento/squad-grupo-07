import React from 'react';
import './styles.css';

const Card = props => {
  return (
    <div
      className={`card ${props.className || ''}`}
      data-aos={props.dataAOS || ''}
      data-aos-delay={props.dataAOSDelay || '0'}
    >
      <div className='card-text'>
        <p>{props.children} </p>
      </div>
      <img src={props.photo} alt='Foto do Usuário' />
      <div className='card-info'>
        <p className='person-name'>{props.name}</p>
        <p className='person-bg'>{props.personBG}</p>
        <div>
          <i className='fas fa-star' />
          <i className='fas fa-star' />
          <i className='fas fa-star' />
          <i className='fas fa-star' />
          <i className='fas fa-star' />
        </div>
      </div>
    </div>

  );
};

export default Card;
