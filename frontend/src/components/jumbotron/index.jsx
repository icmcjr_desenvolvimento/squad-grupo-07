import React from 'react';
import './styles.css';

import LinkButton from '../linkbutton/index';

const Jumbotron = props => (
  <div
    className='jumbotron'
    data-aos='slide-up'
  >
    <h1>{props.title}</h1>
    <LinkButton href='/app'>Conheça Já</LinkButton>
  </div>
);

export default Jumbotron;
