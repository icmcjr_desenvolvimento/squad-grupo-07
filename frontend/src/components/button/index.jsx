import React from 'react';
import If from '../if/index';

export default props => (
  <If test={!props.hide}>
    <button
      className={props.className}
      onClick={props.onClick}
    >{props.children}
    </button>
  </If>
);
