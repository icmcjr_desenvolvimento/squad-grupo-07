import React from 'react';

import './styles.css';
import Emoji from '../../components/emoji/index';
import Face from '../../imgs/face_logo.png';
import Insta from '../../imgs/instagram_logo.png';
import Linkedin from '../../imgs/linkedin_logo.png';
import Google from '../../imgs/google_logo.png';

const Footer = (props) => {
  return (
    <div className='pag-footer'>
      <div
        className='web-links'
        data-aos='fade-right'
      >
        <a href='https://www.facebook.com/ICMCJunior' target='_blank' rel='noopener noreferrer'>
          <img src={Face} alt='facebook' />
        </a>
        <a href='https://instagram.com/icmcjunior?igshid=1v2yh3ekkc188' target='_blank' rel='noopener noreferrer'>
          <img src={Insta} alt='instagram' />
        </a>
        <a href='https://www.linkedin.com/company/731515' target='_blank' rel='noopener noreferrer'>
          <img src={Linkedin} alt='likedin' />
        </a>
        <a href='https://icmcjunior.com.br/' target='_blank' rel='noopener noreferrer'>
          <img src={Google} alt='google-pag' />
        </a>

      </div>

      <h4
        className='footer-description'
        data-aos='fade-left'
        data-aos-delay='500'
        data-aos-anchor='.web-links'
      >
        <pre>Feito com <Emoji symbol='💙' label='blue-heart' /> ICMC Jr</pre>
      </h4>

    </div>
  );
};

export default Footer;
