import React from 'react';

import './styles.css';

const TaskH = (props) => {
  return (
    <div
      className='task-header'
      style={{
        backgroundColor: props.color
      }}
    >
      <div className='task-title'>
        <h3 className='task-title-content'>{props.taskName}</h3>

      </div>
      <div className='task-header-body'>
        <p className='task-date'>Para: {props.date}</p>
        <button
          onClick={props.handleFunction}
          className='edit'
          style={{
            opacity: '0'
          }}
        >
          Edite
        </button>
      </div>
    </div>);
};

export default TaskH;
