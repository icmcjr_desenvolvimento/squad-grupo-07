import React from 'react';

import './styles.css';
import Flaticon from '../../imgs/phone_task_flaticon.png';

const Presentation = props => (
  <>
    <div id='about' className='presentation'>

      <h3>VOCÊ QUER TER CONTROLE?</h3>
      <p>
        Seu cotidiano se tornou extremamente dinâmico devido à toda conectividade
        fornecida hoje pelas novas tecnologias, todas ao alcance de um click. Em meio a tantas
        distrações é muito fácil perder o foco sobre o que é preciso ser feito. Queremos ajudá-lo
        a recuperar o controle da sua vida.
      </p>
      <p>
        Oferecemos a você uma ferramenta projetada para ser simples e te trazer conforto: o
        administrador de tarefas ideal, com um design que te lembra a boa e velha lista de
        afazeres, com uma implementação fácil e prática que te permite planejar a sua rotina sem
        gastar tempo e não perder de vista suas tarefas .
      </p>
      <p id='frase-efeito'>
        Não se deixe ser carregado pela onda de informações da internet,
        retome agora o controle da sua vida.
      </p>
    </div>

    <div className='phone-img'>
      <img src={Flaticon} alt='phone_task_image' />
    </div>
  </>
);

export default Presentation;
