import React from 'react';
import If from '../if/index';
import './styles.css';

// div que age como botão
// Capaz de receber um href
export default props => (
  <If test={!props.hide}>
    <a href={props.href} className={`navlink ${props.className || ''}`}>
      <div>
        {props.children}
      </div>
    </a>
  </If>
);
