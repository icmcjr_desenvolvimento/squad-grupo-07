import React from 'react';
import './styles.css';

export default props => {
  return (
    <div
      className={`list-block ${props.selected || ''}`}
      style={{
        height: props.height || '10%'
      }}
      {... (props.onFunction ? { onClick: props.onFunction } : {})}
    >
      <div
        className='block-title'
        style={{
          backgroundColor: (props.color || 'initial'),
          borderRadius: '20px',
          padding: '5px'
        }}
      >
        {props.isFinished ? (<strike>{props.description}</strike>) : props.description}
      </div>
      <div className='block-secondary'>{props.secondary}</div>
    </div>
  );
};
