import React from 'react';
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom';

import App from '../../pages/app';
import Landing from '../../pages/landing';

export default () => (
  <BrowserRouter>
    <Switch>
      <Route path='/app' component={App} />
      <Route exact path='/' component={Landing} />
      <Redirect from='*' to='/' />
    </Switch>
  </BrowserRouter>
);
