import React from 'react';
import NavLink from '../../components/linkbutton/index';
import '@fortawesome/fontawesome-free/js/all';
import './styles.css';

const TodoMenu = () => {
  return (
    <nav className='navbar'>
      <a href='/#'>
        <div className='nav-item logo'>
          <i className='far fa-list-alt fa-3x' />
          <p>TodoApp</p>
        </div>
      </a>
      <NavLink href='/#about' className='margin-left nav-item'>Sobre</NavLink>
      <NavLink href='/#ratings' className='nav-item'>Opiniões</NavLink>
      <NavLink href='/app' className='nav-item call-to-action'>Começe a Usar</NavLink>
    </nav>
  );
};

export default TodoMenu;
