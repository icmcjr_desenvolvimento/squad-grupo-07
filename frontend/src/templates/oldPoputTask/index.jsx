import React from 'react';

// Task popUp must alow the user to change some task attributes as :
//  Name, color, description
export default (props) => (
  <div className='task-popup-container'>
    <div className='task-popUP'>
      <form action='#' method='put'>
        <fieldset>
          <legend>Altere sua tarefa aqui</legend>

          <div>
            <label htmlFor='new-name'>Nome: </label><br />
            <input
              type='text' name='new-name' id='task-new-name'
              size='50' maxLength='50' placeholder='Insira o nome da sua tarefa aqui ...'
            />
          </div>

          <div>
            <label htmlFor='priority'>
              Prioridade:
              <select name='priority' id='task-priority'>
                <option value='1'>Baixa</option>
                <option value='2'>Média</option>
                <option value='3'>Alta</option>
              </select>
            </label>
          </div>

          <div>
            <label htmlFor='end-line'>
              Data para fim da tarefa:
              <input type='date' name='end-line' id='date' />
            </label>

          </div>

          <div>
            <label htmlFor='description'>
              <h3 id='list-description-title'>Descrição:</h3>
              <textarea
                name='description' id='task-description'
                cols='60' rows='10'
                placeholder='Insira a descrição para a sua tarefa aqui...'
              />
            </label>
          </div>
        </fieldset>
      </form>
    </div>
    <div className='utilities-popup'>
      <button id='popupFinish' onClick={props.onClick}>
        Finalizar
      </button>
    </div>
  </div>
);
