import React from 'react';
import './styles.css';
import './listPopupStyle.css';

// Popup
export default (props) => (
  <>
    <div
      class='popup-background'
      style={{
        display: props.show ? 'initial' : 'none'
      }}
    />
    <div
      className='popup-new'
      style={{
        display: props.show ? 'initial' : 'none'
      }}
    >
      <div className='utilities-popup'>
        <div className='task-popup-container'>
          <div className='list-popUP'>
            <form action='#' method='put'>
              <fieldset>
                <legend>Altere sua lista aqui</legend>

                <div>
                  <label htmlFor='new-name'>Nome: </label><br />
                  <input
                    type='text' name='new-name' id='list-new-name'
                    size='50' maxLength='25' placeholder='Insira o nome da sua lista aqui ...'
                  />
                </div>

                <div>
                  <label htmlFor='colors'>
                    Selecione uma cor:
                    <select name='colors' id='list-color'>
                      <option value='#FFDC00'>Amarelo</option>
                      <option value='#FF4136'>Vermelho</option>
                      <option value='#2ECC40'>Verde</option>
                      <option value='#0074D9'>Azul</option>
                      <option value='#F012BE'>Roxo</option>
                      <option value='#FF851B'>Laranja</option>

                    </select>
                  </label>
                </div>
              </fieldset>
            </form>

            <div className='end-of-popUp'>
              <button
                id='popupFinish'
                onClick={
                  () => props.onClick(
                    document.getElementById('list-new-name').value,
                    document.getElementById('list-color').value
                  ) // eslint-disable-line
                }
              >
                Finalizar
              </button>
            </div>

          </div>
        </div>
      </div>
    </div>
  </>
);
