import React from 'react';
import './styles.css';

import Card from '../../components/card';
import foto1 from '../../imgs/foto1-opinioes.jpg';
import foto2 from '../../imgs/foto2-opinioes.jpg';
import foto3 from '../../imgs/foto3-opinioes.jpg';

export default () => (
  <div id='ratings' className='ratings'>
    <Card
      dataAOS='fade-up'
      name='Jolyne Cujoh'
      personBG='Stone Ocean LTDA'
      photo={foto1}
    >
      Estou muito feliz de ter escolhido o TodoApp para minhas listas de tarefas.
      Sinto que agora sou capaz de me organizar como nunca antes!
    </Card>

    <Card
      dataAOS='fade-up'
      dataAOSDelay='250'
      name='Pedro Linkedino'
      personBG='Big Brain Memes Corporation'
      photo={foto2}
    >
      Após usar pessoalmente por 3 meses, eu adquiri o TodoApp para toda a minha empresa! <br />
      Estou muito feliz com os resultados que estamos tendo!
    </Card>

    <Card
      dataAOS='fade-up'
      dataAOSDelay='500'
      name='Aristóteles'
      personBG='Freelancer'
      photo={foto3}
    >
      O TodoApp mudou a minha vida, substituí o GitHub pela plataforma e consigo me lembrar
      de todas as alterações no código mentalmente.
    </Card>
  </div>
);
