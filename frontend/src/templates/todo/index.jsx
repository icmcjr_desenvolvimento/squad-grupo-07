import React, { Component } from 'react';
import axios from 'axios';

import ListHeader from '../../components/listHeader/index';
import TaskHeader from '../../components/taskHeader/index';
import ListBlock from '../../components/listblock';
import PopupTask from '../PopupTask/index';
import PopupList from '../PopupList/index';

import './styles.css';

const API = 'http://localhost:5000/api/';

class Todo extends Component {
  constructor (props) {
    super(props);
    this.state = {
      showListPopup: false,
      showTaskPopup: false,
      currentList: [],
      currentTask: {},
      viewLists: [],
      viewTasks: [],
      currentDescription: 'Selecione uma tarefa para ver sua descrição',
      currentTaskSearch: '',
      currentListSearch: ''
    };
    const user = prompt('Nome do usuário?'); // eslint-disable-line
    this.handleRefreshUserData(user);
  }

  // ################## LÓGICA APENAS PARA O CLIENTE ###############################
  // Usando o número da list, pega todas as tarefas dentro dela e as exibe na segunda coluna
  handleSelectList = (listNumber) => {
    this.setState({
      currentList: this.state.lists[listNumber],
      viewTasks: this.state.lists[listNumber].tasks,
      currentTaskSearch: ''
    });
  }

  // Seleciona uma tarefa específica e exibe a descrição dela na terceira coluna
  handleSelectTask = async (taskNumber) => {
    await this.setState({
      currentTask: {
        ...this.state.currentList.tasks[taskNumber]
      },
      currentDescription: this.state.currentList.tasks[taskNumber].taskDescription
    });

    // console.log(this.state.currentTask)
  }

  // Serve pro botão "Selecionar todas as tarefas"
  // Seleciona todas as tarefas, dentre todas as lists
  handleSelectAllTasks = () => {
    let viewTasks = []; // Tarefas encontradas

    this.state.lists.forEach((list) => { // Loop que concatena cada list de tarefas na variável anterior
      viewTasks = viewTasks.concat(list.tasks);
    });

    this.setState({ /* Define o nome da list atual como Todas as Tarefas
                       e adiciona as tarefas na currentList (list ativa) */
      currentList: {
        _id: 1,
        listTitle: 'Todas as tarefas',
        listColor: 'var(--secondary)',
        tasks: viewTasks
      },
      viewTasks, // viewTasks são todas as tarefas que vão aparecer na segunda coluna, atualizamos ela também
      currentTaskSearch: '' // Zera a barra de pesquisa de tarefas
    });
  }

  // Ouve todas as mudanças que acontecem na barra de pesquisas da list
  // Após isso, filtra pra ver se encontra alguma list onde apareça a string procurada
  handleListInputChange = (event) => {
    const currentListSearch = event.target.value; // Define o value (dados do input)
    let viewLists = this.state.lists; // Declara uma segunda variável com todas as lists

    // Filtra as lists, através da busca do input que aparece na tela
    viewLists = viewLists.filter((list) => (
      list.listTitle.toLowerCase().includes(currentListSearch.toLowerCase())
    ));

    // Renova o estado da aplicação com a nova view
    this.setState({
      viewLists,
      currentListSearch
    });
  }

  // Ouve todas as mudanças na barra de pesquisa das tarefas
  // Após isso, filtra as tarefas usando a descrição e título delas
  // Equivale a handleListInputChange, porém usando a barra de pesquisa da segunda coluna
  handleTaskInputChange = (event) => {
    const currentTaskSearch = event.target.value;
    let viewTasks = this.state.currentList.tasks;

    viewTasks = viewTasks.filter((tarefa) => (
      tarefa.taskTitle.toLowerCase().includes(currentTaskSearch.toLowerCase()) ||
      tarefa.taskDescription.toLowerCase().includes(currentTaskSearch.toLowerCase())
    ));

    this.setState({
      viewTasks,
      currentTaskSearch
    });
  }

  // ######################### LOGICA ENVOLVENDO CHAMADAS NA API ##################################

  // No momento não é utilizada
  // Serve para iniciar o state, usando o ID do usuário para pegar todoas as lists na tabela

  // Endpoint: GET /api/users/:userId
  handleRefreshUserData = async (username) => {
    let user = await axios.get(API + 'users/' + username);
    if (user.status === 200) {
      user = await axios.post(API + 'users/', {
        username: username
      });
    }
    await this.setState({
      ...user.data,
      viewLists: user.data.lists
    });
    console.log(this.state);
  }

  // Endpoint: POST /api/lists/:userId
  handleAddList = async (color, title) => {
    const newList = {
      listColor: color || 'var(--secondary)',
      listTitle: title || 'Nova Lista'
    };

    const lists = await axios.post(
      (API + 'lists/' + this.state._id),
      { ...newList }
    );

    this.setState({
      viewLists: lists.data,
      lists: lists.data
    });
  }

  // Endpoint: PUT /api/lists/:userId/:listId
  handleEditList = async (title, color) => {
    // Impede a execução em caso de "Todas as tarefas" selecionada
    if (this.state.currentList._id === 1) { return; }
    // Testatndo o Popup

    const editedList = {
      listColor: color || this.state.currentList.listColor,
      listTitle: title || this.state.currentList.listTitle
    };

    const lists = await axios.put(
      (API + 'lists/' + this.state._id + '/' + this.state.currentList._id),
      {
        ...editedList
      });

    this.setState({
      viewLists: lists.data,
      lists: lists.data
    });

    this.handleShowListPopup();
  }

  // Endpoint: DELETE /api/lists/:userId/:listID
  handleRemoveList = async () => {
    // Impede a execução em caso de "Todas as tarefas" selecionada
    if (this.state.currentList._id === 1) { return; }

    const lists = await axios.delete(
      (API + 'lists/' + this.state._id + '/' + this.state.currentList._id));

    this.setState({
      viewLists: lists.data,
      lists: lists.data
    });

    this.handleSelectAllTasks();
  }

  // Endpoint: POST /api/tasks/:userId/:listID
  handleAddTask = async (description, priority, date, title) => {
    if (this.state.currentList._id === 1) { return; }

    const newTask = {
      taskDescription: description || 'Escreva o que pretende fazer aqui',
      taskPriority: priority || 1,
      taskDate: date || Date.now,
      taskTitle: title || 'Nova tarefa',
      taskConcluded: 0
    };

    const lists = await axios.post(
      (API + 'tasks/' + this.state._id + '/' + this.state.currentList._id),
      { ...newTask }
    );

    let i = 0;

    lists.data.forEach((list, key) => {
      if (list._id === this.state.currentList._id) { i = key; }
    });

    this.setState({
      viewTasks: lists.data[i].tasks,
      currentList: lists.data[i],
      lists: lists.data,
      currentTaskSearch: ''
    });
  }

  // Endpoint: PUT /api/tasks/:userID/:listID/:taskID
  handleEditTask = async (title, priority, date, description, concluded) => {
    if (this.state.currentList._id === 1) { return; } // Impede a execução em caso de Todas as Tarefas selecionadas
    if (!this.state.currentTask) { return; }

    const editTask = {
      taskDescription: description || this.state.currentDescription,
      taskPriority: priority || this.state.currentTask.taskPriority,
      taskDate: date || this.state.currentTask.taskDate,
      taskTitle: title || this.state.currentTask.taskTitle,
      taskConcluded: concluded === null ? 0 : concluded
    };

    const lists = await axios.put((
      API + 'tasks/' + this.state._id +
      '/' + this.state.currentList._id +
      '/' + this.state.currentTask._id
    ),
    { ...editTask }
    );

    let [i, j] = [0, 0];
    lists.data.forEach((list, key1) => {
      if (list._id === this.state.currentList._id) {
        i = key1;
        lists.data[i].tasks.forEach((task, key2) => {
          if (task._id === this.state.currentTask._id) {
            j = key2;
          }
        });
      }
    });

    this.setState({
      lists: lists.data,
      currentList: lists.data[i],
      currentTask: lists.data[i].tasks[j],
      viewTasks: lists.data[i].tasks,
      currentDescription: lists.data[i].tasks[j].taskDescription
    });

    await this.setState({
      showTaskPopup: false
    });
  }

  // Endpoint: DELETE /api/tasks/:userId/:listID/:taskID
  handleRemoveTask = async () => {
    if (this.state.currentList._id === 1) { return; }
    if (!this.state.currentTask) { return; }

    const lists = await axios.delete(
      API + 'tasks/' + this.state._id +
      '/' + this.state.currentList._id +
      '/' + this.state.currentTask._id
    );

    let i = 0;
    lists.data.forEach((list, key1) => {
      if (list._id === this.state.currentList._id) {
        i = key1;
      }
    });

    this.setState({
      lists: lists.data,
      currentList: lists.data[i],
      currentTask: {},
      viewTasks: lists.data[i].tasks,
      currentDescription: 'Selecione uma tarefa para visualizar sua descrição!'
    });
  }

  // #################### FINALIZAÇÃO DE TAREFA ###################
  onConclusion = (tasks) => {
    let total = 0;
    let concluidas = 0;

    tasks.forEach(task => {
      if (task.taskConcluded) {
        total++;
        concluidas++;
      } else {
        total++;
      }
    });

    return `${concluidas}/${total}`;
  }

  // Finaliza uma tarefa
  // Adiciona uma linha nas palavras, evidenciando a sinalização
  // Isso ocorre através de uma renderização condicional no componente ListBlock
  handleFinishTask = async (event, i) => {
    const currentList = this.state.currentList;
    // Se não estiver concluida, conclui
    // Se ja estiver, "desconclui"
    currentList.tasks[i].taskConcluded = !currentList.tasks[i].taskConcluded;

    await this.handleSelectTask(i);
    await this.setState({
      currentList
    });

    await this.handleEditTask(0, 0, 0, 0, currentList.tasks[i].taskConcluded);
    // Evita com que o apertar do botão troque a tarefa visualizada atualmente

    // Maior gambiarra da minha vida, deu ruim
    const fakeEvent = {
      target: {
        value: this.state.currentListSearch
      }
    };
    await this.handleListInputChange(fakeEvent);
  }

  // #################### Funções do Popup ########################################
  handleShowListPopup = async () => {
    if (this.state.currentList === 1) { return; }
    if (!this.state.showTaskPopup) {
      await this.setState({
        showListPopup: !this.state.showListPopup
      });
      document.getElementById('list-new-name').value = this.state.currentList.listTitle;
      document.getElementById('list-color').value = null;
    }
  }

  handleShowTaskPopup = async () => {
    if (this.state.currentList._id === 1) { return; }
    if (!this.state.showListPopup) {
      this.setState({
        showTaskPopup: !this.state.showTaskPopup
      });
      document.getElementById('task-description').value = this.state.currentTask.taskDescription;
      document.getElementById('task-new-name').value = this.state.currentTask.taskTitle;
      document.getElementById('task-priority').value = null;
      document.getElementById('date').value = '';
    }
  }

  // ################################### Renderização #######################################
  render = () => {
    return (
      <>
        <PopupTask
          show={this.state.showTaskPopup}
          onClick={this.handleEditTask}
        />
        <PopupList
          show={this.state.showListPopup}
          popupTitle=''
          popupContent
          onClick={this.handleEditList}
        />
        <div className='app-container' data-aos='fade-in'>

          <div className='lists options'>
            <ListHeader
              onAdd={() => this.handleAddList()}
              onEdit={this.handleShowListPopup}
              onRemove={this.handleRemoveList}
              onSearch={this.handleSearchList}
              onInputChange={this.handleListInputChange}

              content={this.state.username}
              inputValue={this.state.currentListSearch}
            />
            {/* Aqui região para Controle de conta<br /> e criação / exclusão de lists<br />Também pesquisa de lists */}
          </div>

          <div className='lists section'>

            {/* Ao entrar na pagina, axios.get(api/_usuario)<br />
            setState(...resultado_api)<br />
            Clicou em uma das lists: <br />
            axios.get(api/id_list)<br />
            setState(list.info)<br />
            Informações exibida sobre a list: <br />
            Nome da list / Cor da list / Realiazadas / Total */}

            <ListBlock description='Todas as tarefas' color='aliceblue' onFunction={this.handleSelectAllTasks} />
            {this.state.viewLists.map((list, i) => (
              <ListBlock
                value={i}
                key={list._id}
                description={list.listTitle}
                secondary={this.onConclusion(list.tasks)}
                color={list.listColor}
                onFunction={() => this.handleSelectList(i)}
              />
            ))}
          </div>

          <div className='tasks options'>
            <ListHeader
              onAdd={() => this.handleAddTask()}
              onEdit={this.handleShowTaskPopup}
              onRemove={this.handleRemoveTask}
              onSearch={this.handleSearchTask}
              onInputChange={this.handleTaskInputChange}

              inputValue={this.state.currentTaskSearch}
              content={this.state.currentList.listTitle}

              color={this.state.currentList.listColor}
            />
            {/* Aqui região pra adicionar e excluir tarefas<br />E também sua pesquisa */}
          </div>

          <div className='tasks section'>

            {/* Clicou em uma das tarefas: <br />
            setState(tarefa_atual = id_tarefa_clicada)<br />
            Informações exibidas sobre a tarefa:<br />
            Prioridade (Cor da tarefa) / Nome / Status / (Fotinha ou Emoji) ???? */}

            {this.state.viewTasks
              .sort((a, b) => a.taskDate > b.taskDate)
              .sort((a, b) => a.taskPriority < b.taskPriority)
              .map((task, i) => (
                <ListBlock
                  value={i}
                  key={task._id}
                  description={task.taskTitle}
                  secondary={
                    <button
                      value={i}
                      style={{
                        backgroundColor: task.taskConcluded ? '#65c565' : '#9d113b'
                      }}
                      onClick={(e) => this.handleFinishTask(e, i)} className='finished'
                    >
                      <i class='fas fa-check' />
                    </button>
                  }
                  color={task.taskPriority === 1 ? ' #0074D9' : task.taskPriority === 2 ? ' #FF851B' : ' #FF4136'}
                  onFunction={() => this.handleSelectTask(i)}
                  isFinished={task.taskConcluded}
                  height='15%'
                />
              ))}

            {/* <ListBlock name='Tarefa 1' secondary={<button>Finalizado</button>} color='red' height='15%' symbol='🙁' />
            <ListBlock name='Tarefa 2' secondary={<button>Finalizado</button>} color='lightblue' height='15%' symbol='🙁' /> */}
          </div>

          <div className='content options'>
            <TaskHeader
              taskName={this.state.currentTask.taskTitle || 'Escolha uma tarefa'}
              date={
                (() => {
                  if (this.state.currentTask.taskDate !== undefined) {
                    const date = new Date(this.state.currentTask.taskDate);
                    const parsedDate = (`${date.getDate() + 1}/${date.getMonth() + 1}/${date.getFullYear()}`);

                    return parsedDate;
                  }
                })()
              }
              color={this.state.currentTask.taskPriority === 1 ? '#0074D9'
                : this.state.currentTask.taskPriority === 2 ? '#FF851B'
                  : this.state.currentTask.taskPriority === 3 ? '#FF4136' : 'var(--secondary)'}
            />
            {/* Aqui ficam as informações sobre a tarefa atual<br />
            Muda de cor dependendo da importância da tarefa<br />
            Opção de salvar as mudanças na tarefa com axios.put(api/id_list/id_tarefa) */}
          </div>

          <div className='content section'>
            {/* Conteúdo da tarefa atual <br />
            Aqui na parte de cima vai ter a edição de algumas partes da tarefa
            editável pelo usuário, precisando da confirmação acima <br />
            Futuramente vou implementar um conversor de Markdown pro aplicativo
            */}

            {this.state.currentDescription}
          </div>
        </div>
      </>
    );
  }
}

export default Todo;
