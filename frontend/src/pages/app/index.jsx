import React, { Component } from 'react';

import Navbar from '../../templates/navbar';
import Todo from '../../templates/todo';
import Footer from '../../components/footer/index';
import './styles.css';

class App extends Component {
  render () {
    return (
      <>
        <div className='background'>
          <Navbar />
          <Todo />
        </div>
        <Footer />
      </>
    );
  }
}

export default App;
