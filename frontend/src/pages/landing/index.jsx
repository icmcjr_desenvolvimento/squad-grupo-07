import React from 'react';
import './styles.css';

import Navbar from '../../templates/navbar';
import Jumbotron from '../../components/jumbotron';
import Presentation from '../../components/presentation';
import Ratings from '../../templates/ratings';
import Footer from '../../components/footer/index';

const Landing = () => {
  return (
    <>
      <Navbar />
      <div className='home-container'>
        <Jumbotron title='A melhor lista de tarefas que voce já viu!' />
      </div>
      <div className='advantages'>
        <Presentation />
      </div>
      <Ratings />
      <Footer />
    </>
  );
};

export default Landing;
