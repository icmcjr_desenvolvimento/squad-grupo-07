import React from 'react';
import ReactDOM from 'react-dom';
import './styles.css';
import AOS from 'aos';
import 'aos/dist/aos.css';

import Routes from './templates/routes/index';

AOS.init({
  duration: 1000,
  once: true
});

ReactDOM.render(
  <> {/* Sintaxe Comprimida para fragmentos react */}
    <Routes />
  </>
  , document.getElementById('root')
);
